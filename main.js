'use strict'
const net = require('net');

const server = net.createServer((socket) => {
    let ch = clientHandler(socket);
    let isDone = false;

    socket.write('\r\nDistance calculator\r\n');
    ch.next();
    socket.on('data', (data) => {
        isDone = ch.next(data.toString()).done;
        if(isDone){
            socket.end('\r\n\nBye!\r\n');
            socket.destroy();
        }
    });
}).on('error', (err) => {
    throw err;
});

server.listen(1234,() => {
    console.log('Server running on ', server.address());
});

/**
 * Generator function that handles the phases of data exchange
 * 
 * About generator functions:
 * "Generators are functions which can be exited and later re-entered.
 * Their context (variable bindings) will be saved across re-entrances."
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function%2A
 */
function* clientHandler(socket){
    let latitude = [];
    let longitude = [];
    let input = undefined;

    for(let i = 0; i < 2; i++){
        while(typeof input !== 'number' || !input) {
            socket.write('\r\nLatitude ' + i + ' (Decimal degrees)> ');
            input = parseFloat(yield);
        }
        latitude.push(input);
        input = undefined;
        while(typeof input !== 'number' || !input) {
            socket.write('\r\nLongitude ' + i + ' (Decimal degrees)> ');
            input = parseFloat(yield);
        }
        longitude.push(input);
        input = undefined;
    }
    socket.write('\r\n');
    for(let i = 0; i < 2; i++){
        socket.write('\r\nLatitude ' + i + ' (Decimal degrees): ' + latitude[i]);
        socket.write('\r\nLongitude ' + i + ' (Decimal degrees): ' + longitude[i]);
    }
    socket.write('\r\nDistance (meters): ' + calculateDistance(
        {latitude: latitude[0], longitude: longitude[0]},
        {latitude: latitude[1], longitude: longitude[1]}
    ).toString());
    return;
}

function calculateDistance(pointA, pointB){
    const R = 6371e3; // Earth's radius in metres
    const latA = toRadians(pointA.latitude);
    const latB = toRadians(pointB.latitude);
    const deltaLat = toRadians(pointB.latitude-pointA.latitude);
    const deltaLon = toRadians(pointB.longitude-pointA.longitude);

    // Haversine formula
    const a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) +
            Math.cos(latA) * Math.cos(latB) *
            Math.sin(deltaLon/2) * Math.sin(deltaLon/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    const d = R * c;
    return d;
}

function toRadians(degrees) {
    return degrees * Math.PI / 180;
}